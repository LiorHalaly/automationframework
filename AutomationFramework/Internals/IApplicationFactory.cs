﻿using OpenQA.Selenium;

namespace AutomationFramework.Internals
{
    public interface IApplicationFactory
    {
        T ChangeContext<T>(IWebDriver driver, string application) where T : class;
        void OpenApplication(IWebDriver driver = null, string application = null);
    }
}