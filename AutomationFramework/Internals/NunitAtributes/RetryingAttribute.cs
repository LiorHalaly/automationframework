﻿using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationInfrastructure.Internals.NunitAtributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class RetryingAttribute : Attribute, IWrapSetUpTearDown
    {
        public int Times { get; set; } = 1;

        public TestCommand Wrap(TestCommand command)
        {
            return new RetryingCommand(command, Times);
        }
    }
}
