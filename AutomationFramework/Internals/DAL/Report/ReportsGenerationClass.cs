﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using System;
using System.IO;

namespace AutomationFramework
{
    public class ReportsGenerationClass
    {
        private static string _reportPath;
        private static ExtentReports _extent;
        protected ExtentTest _test;
        protected DateTime time = DateTime.Now;
        protected static string _screenShotName;
        private Status _logstatus;
        private static ExtentHtmlReporter _htmlReporter;

        public static ExtentReports ReportSetUp()
        {
            if (_extent == null)
            {
                var file = @"\Jenkins\workspace\Donet\TestProject\";
                var absolute_path = $"{Environment.GetEnvironmentVariable("ProgramFiles(x86)")}{file}";              
                //var path = System.Reflection.Assembly.GetCallingAssembly().CodeBase;
                //var actualPath = path.Substring(0, path.LastIndexOf("bin"));
                var projectPath = new Uri(absolute_path).LocalPath;
                var date = DateTime.Now.ToString("dd.MM.yyyy");
                var time = DateTime.Now.ToString("HH.mm");
                var format = "{0} on {1} At {2}";
                var folderName = string.Format(format, "Report", date, time);
                _reportPath = Path.Combine(projectPath + "Reports", folderName, "Screenshots");
                Directory.CreateDirectory(_reportPath);
                _htmlReporter = new ExtentHtmlReporter(_reportPath);
                _extent = new ExtentReports();
                _extent.AttachReporter(_htmlReporter);
                _extent.AddSystemInfo("Host Name", "LocalHost");
                _extent.AddSystemInfo("Environment", "QA");
                _extent.AddSystemInfo("UserName", "TestUser");
            }
            return _extent;
        }

        public void BeforeTest()
        {
            _test = _extent.CreateTest(TestContext.CurrentContext.Test.Name);
        }

        public void ReportTearDoun()
        {
            _extent.Flush();
        }

        public void AfterTest(IWebDriver driver)
        {
            try
            {
                var status = TestContext.CurrentContext.Result.Outcome.Status;
                var testName = TestContext.CurrentContext.Test.Name;
                var testMessage = TestContext.CurrentContext.Result.Message;
                var testAsserts = TestContext.CurrentContext.Result.Assertions;
                var stackTrace = "<pre>" + TestContext.CurrentContext.Result.StackTrace + "</pre>";

                switch (status)
                {
                    case TestStatus.Failed:
                        FailCase(driver, testName);
                        break;

                    case TestStatus.Inconclusive:
                        _logstatus = Status.Warning;
                        break;

                    case TestStatus.Skipped:
                        _logstatus = Status.Skip;
                        break;

                    default:
                        _logstatus = Status.Pass;
                        break;
                }
                _test.Log(_logstatus, $" Test ended with {_logstatus} {stackTrace} {testMessage} {testAsserts}");
                _extent.Flush();
            }
            catch { }
        }

        private static string Capture(IWebDriver driver, String screenShotName)
        {
            ITakesScreenshot ts = (ITakesScreenshot)driver;
            Screenshot screenshot = ts.GetScreenshot();
            screenshot.SaveAsFile(Path.Combine(_reportPath, screenShotName), ScreenshotImageFormat.Png);
            return _reportPath;
        }

        private void FailCase(IWebDriver driver, string testName)
        {
            _logstatus = Status.Fail;
            _screenShotName = "Screenshot_" + DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss") + ".png";
            String screenShotPath = Capture(driver, _screenShotName);
            _test.Log(Status.Fail, "Fail");
            _test.Log(Status.Fail, "Snapshot below: " + _test.AddScreenCaptureFromPath(Path.Combine(screenShotPath, _screenShotName)));
            _test.Log(Status.Fail, testName);
        }      
    }

}