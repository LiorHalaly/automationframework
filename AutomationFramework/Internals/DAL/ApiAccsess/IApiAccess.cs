﻿using System.Threading.Tasks;

namespace AutomationFramework.Internals.DAL
{
    public interface IApiAccess
    {
        Task<TResponseDto> ExecuteGetEntry<TResponseDto>(string apiRoute);
    }
}