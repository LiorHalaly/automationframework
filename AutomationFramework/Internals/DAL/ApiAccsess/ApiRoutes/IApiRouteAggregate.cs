﻿namespace AutomationFramework.ApiRoutes
{
    public interface IApiRouteAggregate
    {
        string GetWeatherEntry();
    }
}