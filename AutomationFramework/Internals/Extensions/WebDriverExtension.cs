﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;

namespace AutomationFramework.WebDriverActions
{
    public static class WebDriverExtension
    {
        public static IWebElement GetElement(this IWebDriver driver, By by, int fromSeconds = 70)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(fromSeconds));
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException),
                typeof(ElementNotVisibleException),
                typeof(ElementNotInteractableException));

            var element = wait.Until(d =>
            {
                try
                {
                    for (int i = 0; i < fromSeconds; i++)
                    {
                        driver.GetWaitForPageToLoad();
                        IWebElement elementToBeDisplayed = driver.FindElement(by);
                        //string js = string.Format("window.scroll(0, {0});", elementToBeDisplayed.Location.Y);
                        //((IJavaScriptExecutor)driver).ExecuteAsyncScript("document.getElementById('text-8').scrollIntoView(true);", elementToBeDisplayed);
                        driver.ExecuteJavaScript("arguments[0].scrollIntoView(false);", elementToBeDisplayed);

                        // enter to stail element exception
                        for (int j = 0; j < 4; j++)
                        {
                            var text = elementToBeDisplayed.Text;
                        }

                        if (elementToBeDisplayed.Displayed && elementToBeDisplayed.Enabled)
                        {
                            return elementToBeDisplayed;
                        }
                    }
                    return null;
                }
                catch (StaleElementReferenceException)
                {
                    Thread.Sleep(400);
                    IWebElement elementToBeDisplayed = driver.FindElement(by);
                    return elementToBeDisplayed;
                }
            });
            return element;
        }

        public static void WaitForElementToBeEnable(this IWebDriver driver, By by, int fromSeconds = 50)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(fromSeconds));

            wait.Until(d =>
            {
                try
                {
                    var elementEnabled = driver.FindElement(by);
                    if (elementEnabled.Enabled)
                    {
                        return true;
                    }

                    return false;
                }
                catch
                {
                    return false;
                }
            });
        }

        public static void ForceClick(this IWebElement webElement, IWebDriver driver, int fromSeconds = 30)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(fromSeconds));

            wait.Until(d =>
            {
                try
                {
                    IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;

                    jse.ExecuteScript("scroll(250, 0)");
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            });
        }

        public static string GetElementText(this IWebElement webElement, IWebDriver driver)
        {
            //IJavaScriptExecutor js = (IJavaScriptExecutor)((RemoteWebElement)webElement).WrappedDriver;

            return driver.ExecuteJavaScript<string>("return arguments[0].innerText;", webElement);
        }

        public static void GetSendsKey(this IWebElement webElement, IWebDriver driver, string text)
        {
            driver.ExecuteJavaScript("arguments[0].setAttribute('value', 'arguments[1]')", webElement, text);
        }

        public static void GetElementWitJavaScript(this IWebDriver driver, By by)
        {
            var singleResult = driver.ExecuteJavaScript<IWebElement>("return $('.my-class')[0]");

            IWebElement elementToPass = driver.FindElement(by);
            var results = driver.ExecuteJavaScript<IReadOnlyCollection<IWebElement>>(
                        "return $(arguments[0]).children('.my-class').toArray()", elementToPass);
        }

        public static void GetSwichtToAlert(this IWebDriver driver)
        {
            IAlert a = driver.SwitchTo().Alert();
            a.Accept();
            a.Dismiss();
        }

        public static void GetSwitchBetweenBrowsersTabs(this IWebDriver driver)
        {
            ReadOnlyCollection<string> windowHandles = driver.WindowHandles;
            string firstTab = windowHandles.First();
            string lastTab = windowHandles.Last();
            driver.SwitchTo().Window(lastTab);
        }

        public static void GetAddCookie(this IWebDriver driver, string key, string value)
        {
            Cookie cookie = new Cookie(key, value);
            driver.Manage().Cookies.AddCookie(cookie);
        }

        public static void GetAllCookies(this IWebDriver driver)
        {
            var cookies = driver.Manage().Cookies.AllCookies;
        }

        public static void GetDeleteCookieByName(this IWebDriver driver, string cookieName)
        {
            driver.Manage().Cookies.DeleteCookieNamed(cookieName);
        }

        public static void GetDeleteAllCookies(this IWebDriver driver)
        {
            driver.Manage().Cookies.DeleteAllCookies();
        }

        public static void GetFullScreenshot(this IWebDriver driver, string pathToSaveImage)
        {
            Screenshot screenshot = ((ITakesScreenshot)driver).GetScreenshot();
            screenshot.SaveAsFile(pathToSaveImage, ScreenshotImageFormat.Png);
        }

        public static void GetWaitForPageToLoad(this IWebDriver driver)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.Until((x) =>
            {
                return ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete");
            });
        }
    }


}