﻿
using AutomationFramework.Internals;

namespace AutomationFramework.Weather
{
    public interface IWeatherUi : IApplicationFactory, ITemperature<int>
    {
        IWeatherUi SearchLocation(string location);

        IWeatherUi ChooseLocation();
    }
}