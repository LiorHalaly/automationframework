﻿namespace AutomationFramework.Weather
{
    public interface ITemperature<T>
    {
        T GetTodayTemperatureValue();
    }
}