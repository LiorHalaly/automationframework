﻿
using AutomationFramework.Dtos;
using AutomationFramework.Internals;

namespace AutomationFramework.Weather
{
    public interface IWeatherApi : IApplicationFactory, ITemperature<GetWeatherResponse>
    {
    }
}