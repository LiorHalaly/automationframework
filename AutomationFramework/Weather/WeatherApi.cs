﻿
using AutomationFramework.ApiRoutes;
using AutomationFramework.Dtos;
using AutomationFramework.Internals;
using AutomationFramework.Internals.DAL;

namespace AutomationFramework.Weather
{
    public class WeatherApi : ApplicationFactory, IWeatherApi
    {
        private IApiAccess _apiAccess;
        private IApiRouteAggregate _apiRouteAggregate;

        public WeatherApi(IApiAccess apiAccess, IApiRouteAggregate apiRouteAggregate)
        {
            _apiAccess = apiAccess;
            _apiRouteAggregate = apiRouteAggregate;
        }

        public GetWeatherResponse GetTodayTemperatureValue()
        {
            var route = _apiRouteAggregate.GetWeatherEntry();
            return _apiAccess.ExecuteGetEntry<GetWeatherResponse>(route).Result;
        }
    }
}