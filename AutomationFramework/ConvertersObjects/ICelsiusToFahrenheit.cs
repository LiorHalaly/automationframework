﻿
using AutomationFramework.Internals;

namespace AutomationFramework.Converters
{
    public interface ICelsiusToFahrenheit : IApplicationFactory
    {
        ICelsiusToFahrenheit ClickOnTemperatureConvector();

        ICelsiusToFahrenheit ClickOnCelsius();

        ICelsiusToFahrenheit ClickOnCelsiusToFahrenheit();

        ICelsiusToFahrenheit TypeToCelsiusTextBox(decimal num);

        ICelsiusToFahrenheit ChangeFormatToDecimal();

        decimal GetConvertionValue();
    }
}