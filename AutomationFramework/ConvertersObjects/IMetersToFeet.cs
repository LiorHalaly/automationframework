﻿
using AutomationFramework.Internals;

namespace AutomationFramework.Converters
{
    public interface IMetersToFeet : IApplicationFactory
    {
        IMetersToFeet ClickOnLengthConvector();

        IMetersToFeet ClickOnMeters();

        IMetersToFeet ClickOnMetersToFeet();

        IMetersToFeet TypeToMetersTextBox(decimal num);

        IMetersToFeet ChangeFormatToDecimal();

        decimal GetConvertionValue();
    }
}