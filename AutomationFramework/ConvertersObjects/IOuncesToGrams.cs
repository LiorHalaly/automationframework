﻿using AutomationFramework.Internals;
using OpenQA.Selenium;

namespace AutomationFramework.Converters
{
    public interface IOuncesToGrams : IApplicationFactory
    {
        IOuncesToGrams InitiateWebDriver(IWebDriver driver);

        IOuncesToGrams NvigateToConvectorSite(string address);

        IOuncesToGrams ClickOnWeightConvector();

        IOuncesToGrams ClickOnOunces();

        IOuncesToGrams ClickOnOuncesToGrams();

        IOuncesToGrams TypeToOuncesTextBox(decimal num);

        decimal GetConvertionValue();
    }
}