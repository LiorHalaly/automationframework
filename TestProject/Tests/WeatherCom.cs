﻿using AutomationFramework.Internals;
using AutomationFramework.Weather;
using AutomationInfrastructure.Internals.NunitAtributes;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;

namespace Tests.Tests
{
    [TestFixture]
    //[Parallelizable(ParallelScope.All)]
    public class WeatherCom : TestSuitBase
    {          
        [Test]
        //[Retrying(Times = 2)]
        public void GetTodayTemperature()
        {
            _driver = new ChromeDriver();
            var location = "20852";
            var temperatureUi = _apiFactory
                .ChangeContext<IWeatherUi>(_driver, weatherSiteUrl)
                .SearchLocation(location)
                .ChooseLocation()
                .GetTodayTemperatureValue();

            var responce = new ApplicationFactory()
                .ChangeContext<IWeatherApi>()
                .GetTodayTemperatureValue();

            var temperatureApi = responce.Vt1Observation.Temperature;
            Assert.IsTrue(true);
            //Assert.IsTrue(Math.Abs(temperatureUi - temperatureApi) <= 0.1);
        }
    }
}