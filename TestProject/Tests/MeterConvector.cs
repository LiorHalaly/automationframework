using AutomationFramework.ConversionRaitesexpected;
using AutomationFramework.Converters;
using NUnit.Framework;
using System;

namespace Tests
{
    //[Parallelizable(ParallelScope.All)]
    [TestFixture]
    public class MeterConvector : TestSuitBase
    {
        private decimal numberToConvert = 15.2m;

        [Test]
        public void MeterToFeet()
        {
            var resultFromUi = _apiFactory
                 .ChangeContext<IMetersToFeet>(_driver, convectorSiteUrl)
                 .ClickOnLengthConvector()
                 .ClickOnMeters()
                 .ClickOnMetersToFeet()
                 .TypeToMetersTextBox(numberToConvert)
                 .ChangeFormatToDecimal()
                 .GetConvertionValue();

            // calcluationn with formula
            var resultFromCalcuation = ConversionRaitasCalcluation.MetersToFeet(numberToConvert);
            var expected = Math.Abs(resultFromUi - resultFromCalcuation) < 1;

            Assert.IsTrue(expected);
        }
    }
}