using AutomationFramework.ConversionRaitesexpected;
using AutomationFramework.Converters;
using NUnit.Framework;
using System;

namespace Tests
{
    //[Parallelizable(ParallelScope.All)]
    [TestFixture]
    public class WeightConvector : TestSuitBase
    {
        private decimal numberToConvert = 15.2m;

        [Test]
        public void OuncesToGrams()
        {
            var resultFromUi = _apiFactory
                 .ChangeContext<IOuncesToGrams>(_driver, convectorSiteUrl)
                 .ClickOnWeightConvector()
                 .ClickOnOunces()
                 .ClickOnOuncesToGrams()
                 .TypeToOuncesTextBox(numberToConvert)
                 .GetConvertionValue();

            var resultFromCalcuation = ConversionRaitasCalcluation.OunceToGram(numberToConvert);
            var expected = Math.Abs(resultFromUi - resultFromCalcuation);
            Assert.IsTrue(expected < 1);
        }
    }
}