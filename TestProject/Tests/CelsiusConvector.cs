using AutomationFramework.ConversionRaitesexpected;
using AutomationFramework.Converters;
using NUnit.Framework;
using System;

namespace Tests
{
    //[Parallelizable(ParallelScope.All)]
    [TestFixture]
    public class CelsiusConvector : TestSuitBase
    {
        private decimal numberToConvert = 12;

        [Test]
        public void CelsiusToFahrenheit()
        {
            var resultFromUi = _apiFactory
                .ChangeContext<ICelsiusToFahrenheit>(_driver, convectorSiteUrl)
                .ClickOnTemperatureConvector()
                .ClickOnCelsius()
                .ClickOnCelsiusToFahrenheit()
                .TypeToCelsiusTextBox(numberToConvert)
                .ChangeFormatToDecimal()
                .GetConvertionValue();

            var resultFromCalcuation = ConversionRaitasCalcluation.CelsiusToFahrenheit(numberToConvert);
            var expected = Math.Abs(resultFromUi - resultFromCalcuation);
            Assert.IsTrue(expected < 1);
        }
    }
}