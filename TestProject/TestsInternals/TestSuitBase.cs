﻿using AutomationFramework;
using AutomationFramework.Internals;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Tests
{
    public class TestSuitBase : ReportsGenerationClass
    {
        public static ApplicationFactory _apiFactory;
        public IWebDriver _driver;
        public string convectorSiteUrl = "https://www.metric-conversions.org/";
        public string weatherSiteUrl = "https://weather.com/";

        public TestSuitBase()
        {
            //_driver = GetDriver("Chrome");
            _apiFactory = new ApplicationFactory();
        }

        public static void CloseProccess()
        {
            //Process.GetProcesses()
            //    .Where(x => x.ProcessName.IndexOf("chrome", StringComparison.OrdinalIgnoreCase) >= 0)
            //             .ToList()
            //             .ForEach(x => x.Kill());
        }

        //public void GetDriver()
        //{
        //    //switch (driverType)
        //    //{
        //    //    case "Chrome":
        //            //ChromeOptions options = new ChromeOptions();
        //            //options.AddArguments("--ignore-certificate-errors");
        //            //options.AddArguments("--disable-popup-blocking");
        //            //options.AddAdditionalCapability(CapabilityType.Platform, "LINUX", true);
        //            //options.AddAdditionalCapability("applicationName", "Grid capability", true);
        //            //options.AddAdditionalCapability("browser", "chrome");
        //            //options.AddAdditionalCapability("maxInstances", 1, true);
        //            //options.AddAdditionalCapability("seleniumProtocol", "WebDriver", true);
        //            //options.AddAdditionalCapability(CapabilityType.Version, "78.0.3904.70", true);
        //            //options.AddAdditionalCapability("zal", "TEST NAME", true);
        //            //return _driver = new ChromeDriver();
        //            //return _driver = new RemoteWebDriver(new Uri("http://172.28.246.184:4444/wd/hub"), options.ToCapabilities());
        //            //Chrome Options for Adblocker extension
        //            //chromeOptions.AddExtensions(@"C:\Users\lior\source\repos\AttentyPractice\extension_3_60_0_0.crx");

        //            // return _driver = new RemoteWebDriver(new Uri("http://172.28.246.184:4444/wd/hub"), Options);
                     

        //        //default:
        //        //     _driver = new ChromeDriver();
        //    //}
        //}

        [OneTimeSetUp]
        protected void Setup()
        {
            ReportSetUp();
        }

        [SetUp]
        public void SetUp()
        {
            BeforeTest();
            _driver = new ChromeDriver();

        }

        [OneTimeTearDown]
        protected void OneTimeTearDown()
        {
            ReportTearDoun();
            CloseProccess();
            //if (_driver != null)
            //{
            //    _driver.Quit();
            //    _driver = null;
            //}
        }

        [TearDown]
        public void TearDown()
        {
            AfterTest(_driver);
            if (_driver != null)
            {
                _driver.Quit();
                _driver = null;
            }
        }
    }

}